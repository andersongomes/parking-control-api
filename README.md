# Parking Control Api

This project includes an API for managing a parking lot.

## The Project
Create an API to park control (follow rules below):
- Must register check in, check out and payment
- Must not release check out without payment
- Must provide history by plate
The API must respect http status correctly, accept requests and respond to json.

## Tecnologies
- Programming Language: Python :snake:
- Framework: Django :snake:
- SGBD: PostgresSQL :elephant:

## Configuring Project

### Requirements
### SGBD
- Before you start, you need to have a Postgresql server (https://www.postgresql.org/download/) running where the credentials for access are: 
  - User: postgres
  - Pass: 12345
- A bank called "parkercontrol" must be created in this server.

### Python
- Python installation and configuration is required, which can be obtained from the following link: https://www.python.org/downloads/

### Preparing the Project on Windows
```bash
mkir park-control-api
cd park-control-api
git clone https://gitlab.com/andersongomes/parking-control-api.git
python -m venv venv
venv\Scripts\activate
```

### Install the Modules:
```bash
 pip install -r requirements.txt
```
or
```bash
pip install django
pip install djangorestframework
pip install markdown
pip install django-filter
pip install psycopg2
pip install flake8
```
### Modules
- asgiref==3.3.1
- Django==3.1.7
- django-filter==2.4.0
- djangorestframework==3.12.2
- flake8==3.9.0
- Markdown==3.3.4
- mccabe==0.6.1
- psycopg2==2.8.6
- pycodestyle==2.7.0
- pyflakes==2.3.0
- pytz==2021.1
- sqlparse==0.4.1
### Make migrations
```bash
cd api
python .\manage.py migrate
```
### Create Super User (Optional)
```bash
python .\manage.py createsuperuser
```
### Run the Project
```bash
python .\manage.py runserver
```
### Access the interface
```bash
http://localhost:8000/
```

## Routs
 Rout                      |     HTTP (Verb)   |      Description      | 
-------------------------  | ----------------- | --------------------- | 
/parking                   |       GET         | Select All            | 
/parking                   |       POST        | Make Check In         | 
/parking/:id/out           |       PUT         | Make Check Out        | 
/parking/:id/pay           |       PUT         | Make Payment          |    
/parking/:plate            |       GET         | Get History by Plate  |

### Check In
- This is an endpoint post that you send in the body a json in these parameters to register a car in a parking lot
```json
  {"plate":"POV-6138"}
```
- This endpoint returns an int that represents a reservation code. This code will serve to bind the customer to pay and exit the parking lot.
- Rout: /parking
### Pay
- This endpoint is responsible for making it possible to pay for parking. In this action, the parking is marked as paid. You need to pass the reservation code received at check-in.
- Rout: /parking/:id/pay 
### Check Out
- This endpoint is responsible for formalizing the vehicle's exit from the parking lot. You need to pass the reservation code received at check-in. In this action, the vehicle's time in the parking lot, the departure date and an exit confirmation are filled out.
- Rout: /parking/:id/out 
### History
- This endpoint returns a list containing all the records in which the license plate car sent in the request parked in the parking lot. Below is an example of return:
```json
  "[ { id: 26, time: '1 minutes', paid: true, left: true }, { id: 27, time: '12 minutes', paid: true, left: true } ]"
```
- Rout: /parking/:plate 

## Model

All the parking management is persisted in the "core_parking" table whose structure is composed of the following fields:

- id : Reservation code.
- plate : Parked car license plate.
- dt_checkin : Date and time of entry of the car into the parking lot.
- dt_checkout : Date and time of the car's exit from the parking lot.
- paid : Defines whether or not the customer has paid for parking.
- time : Sets how many minutes the car has been parked.
- left : Defines whether or not the customer has left the parking lot.

The another tables are provided by Django.

### ER diagram
![DER](/images/DER.png)

## Tests

### Running tests
```bash
python .\manage.py test
```

### Scenarys
- This photos represent some the implemented tests of this project
- Check In

  ![DER](/images/checkin.png)
- Failure on Check In with malformated plate

  ![DER](/images/checkin_failure_format.png)
- Check In failed with malformed plate with more characters than allowed

  ![DER](/images/checkin_failure_size.png)
- Check-out failed when trying to exit the parking lot without paying

  ![DER](/images/checkout_failure.png)
- Parking payment action

  ![DER](/images/pay.png)
- Check-out action

  ![DER](/images/checkout.png)
- Get the parking history from the license plate.

  ![DER](/images/history.png)
