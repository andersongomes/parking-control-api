from django.contrib import admin
from .models import Parking


class ParkingAdmin(admin.ModelAdmin):
    model = Parking
    list_display = ('id', 'plate', 'dt_checkin',
                    'dt_checkout', 'paid', 'time', 'left')
    list_filter = ('plate', 'dt_checkin', 'dt_checkout',
                   'paid', 'time', 'left')
    search_fields = ('plate', 'dt_checkin',
                     'dt_checkout', 'paid', 'time', 'left')


admin.site.register(Parking, ParkingAdmin)
