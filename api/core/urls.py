from django.urls import path
from core.views import ParkingViewSet
from rest_framework.urlpatterns import format_suffix_patterns

parking_list = ParkingViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
parking_detail = ParkingViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
parking_checkout = ParkingViewSet.as_view({
    'put': 'out'
})
parking_payment = ParkingViewSet.as_view({
    'put': 'pay'
})
parking_history = ParkingViewSet.as_view({
    'get': 'history'
})

urlpatterns = format_suffix_patterns([
    path('parking', parking_list, name='parking_list'),
    path('parking/<int:pk>', parking_detail, name='parking_detail'),
    path('parking/<int:pk>/out', parking_checkout, name='parking_checkout'),
    path('parking/<int:pk>/pay', parking_payment, name='parking_payment'),
    path('parking/<str:plate>', parking_history, name='parking_history')
])
