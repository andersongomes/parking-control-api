from rest_framework import serializers
from .models import Parking
import re


class ParkingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Parking
        fields = '__all__'

    def validate_plate(self, value):
        rule = re.compile(r'[A-Z]{3}-[0-9]{4}')
        if not rule.search(value):
            raise serializers.ValidationError("Invalid Plate")
        return value
