from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Parking
import json


class ParkingTest(APITestCase):

    def test_parking_api(self):
        # in this test we will validate
        # if the endpoint checkin is ok
        url = reverse('parking_list')
        data1 = {'plate': 'POV-6127'}
        response1 = self.client.post(url, data1, format='json')
        self.assertEqual(response1.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Parking.objects.count(), 1)
        self.assertEqual(Parking.objects.get().plate, 'POV-6127')

        # test refers to plate validation
        # Uppercase validation
        data2 = {'plate': 'POv-6127'}
        response2 = self.client.post(url, data2, format='json')
        self.assertEqual(response2.status_code, status.HTTP_400_BAD_REQUEST)

        # Lenght > 8
        data3 = {'plate': 'POV-61270'}
        response3 = self.client.post(url, data3, format='json')
        self.assertEqual(response3.status_code, status.HTTP_400_BAD_REQUEST)

        # Number validation
        data4 = {'plate': 'POV-612A'}
        response4 = self.client.post(url, data4, format='json')
        self.assertEqual(response4.status_code, status.HTTP_400_BAD_REQUEST)

        # History endpoint test with correct key
        response5 = self.client.get(reverse('parking_history',
                                            kwargs={'plate': 'POV-6127'}))
        self.assertEqual(response5.status_code, status.HTTP_200_OK)

        # History endpoint test with wrong key
        response6 = self.client.get(reverse('parking_history',
                                            kwargs={'plate': 'POV-6130'}))
        self.assertEqual(response6.status_code, status.HTTP_400_BAD_REQUEST)

        # Def base payload to put endpoints tests
        self.valid_payload = {}

        # Making check out before pay
        response11 = self.client.put(
            reverse('parking_checkout', kwargs={'pk': 1}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response11.status_code, status.HTTP_400_BAD_REQUEST)

        # Pay endpoint test with correct key
        response7 = self.client.put(
            reverse('parking_payment', kwargs={'pk': 1}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response7.status_code, status.HTTP_200_OK)

        # Pay endpoint test with wrong key
        response8 = self.client.put(
            reverse('parking_payment', kwargs={'pk': 2}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response8.status_code, status.HTTP_400_BAD_REQUEST)

        # Out endpoint test with correct key
        response9 = self.client.put(
            reverse('parking_checkout', kwargs={'pk': 1}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response9.status_code, status.HTTP_200_OK)

        # Pay endpoint test with wrong key
        response10 = self.client.put(
            reverse('parking_checkout', kwargs={'pk': 2}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response10.status_code, status.HTTP_400_BAD_REQUEST)
