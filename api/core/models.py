from django.db import models


class Parking(models.Model):
    plate = models.CharField(max_length=8, null=True)
    dt_checkin = models.DateTimeField(auto_now_add=True)
    dt_checkout = models.DateTimeField(null=True)
    paid = models.BooleanField(default=False)
    time = models.CharField(max_length=50, null=True)
    left = models.BooleanField(default=False)

    class Meta:
        ordering = ['dt_checkin']

    def __str__(self):
        return self.plate
