from .models import Parking
from django.core import serializers
from .serializers import ParkingSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from datetime import datetime
import json
from rest_framework import status
from django.http import Http404
from rest_framework.request import clone_request


class ParkingViewSet(viewsets.ModelViewSet):
    queryset = Parking.objects.all()
    serializer_class = ParkingSerializer

    def perform_create(self, serializer):
        serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data['id'],
                        status=status.HTTP_201_CREATED, headers=headers)

    def out(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        self.object = self.get_object_or_none()
        serializer = self.get_serializer(self.object,
                                         data=request.data, partial=partial)
        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        if self.object is None:
            return Response("Reservation number not found",
                            status=status.HTTP_400_BAD_REQUEST)
        parking = self.get_object()
        if not parking.paid:
            return Response("Checkout is not possible without payment",
                            status.HTTP_400_BAD_REQUEST)
        else:
            dt_checkout_aux = datetime.now()
            dt_checkin_aux = parking.dt_checkin
            permanency_time = dt_checkout_aux - dt_checkin_aux
            serializer.save(dt_checkout=dt_checkout_aux,
                            time=str(int(permanency_time.seconds/60)) +
                            ' minutes', left=True)
            return Response('OK', status=status.HTTP_200_OK)

    def pay(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        self.object = self.get_object_or_none()
        serializer = self.get_serializer(self.object,
                                         data=request.data, partial=partial)
        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        if self.object is None:
            return Response("Reservation number not found",
                            status=status.HTTP_400_BAD_REQUEST)
        serializer.save(paid=True)
        return Response('OK', status=status.HTTP_200_OK)

    def history(self, request, *args, **kwargs):
        plateFilter = self.kwargs['plate']
        parkings = Parking.objects.filter(plate=plateFilter)
        jsonReturn = serializers.serialize('json', parkings,
                                           fields=('id', 'plate', 'time',
                                                   'paid', 'left'))
        preJson = json.loads(jsonReturn)
        retorno = '['
        for i in preJson:
            id = ' { id: ' + str(i['pk']) + ', '
            fields = i['fields']
            if fields['time'] is None:
                time = 'time: \'\', '
            else:
                time = 'time: \'' + fields['time'] + '\', '
            paid = 'paid: ' + str(fields['paid']).lower() + ', '
            left = 'left: ' + str(fields['left']).lower() + ' }'
            retorno += id + time + paid + left + ', '
        if len(retorno) > 2:
            retorno = retorno[:-2] + ' ]'
        else:
            return Response("Plate not find",
                            status=status.HTTP_400_BAD_REQUEST)
        return Response(retorno, status=status.HTTP_200_OK)

    def get_object_or_none(self):
        try:
            return self.get_object()
        except Http404:
            if self.request.method == 'PUT':
                self.check_permissions(clone_request(self.request, 'POST'))
            else:
                raise
